﻿using GrapesIndiaApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Repository
{
    public interface IUserRepository
    {
        Task<User> ValidateUserAsync(string username, string password);
        Task<List<User>> GetAllUserAsync();
    }
}
