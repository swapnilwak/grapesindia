﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Models
{
    public class Gallery
    {
        [Key]
        public int GalleryId { get; set; }
        public string GalleryTitle { get; set; }
        public string GalleryDirName { get; set; }
    }
}
