﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Models
{
    public class Schedule
    {
        [Key]
        public int ScheduleID { get; set; }
        public int DayNo { get; set; }
        public string Phase { get; set; }
        public string Medicine { get; set; }
        public string Component { get; set; }
        public string Use { get; set; }
        public string Mrl { get; set; }
        public string Phi { get; set; }
    }
}
