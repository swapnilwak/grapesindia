﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Models
{
    public class GrapesIndiaContext : DbContext
    {
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<Schedule> Schedule { get; set; }
        public virtual DbSet<Information> Information { get; set; }
        public virtual DbSet<News> NewsTable { get; set; }
        public virtual DbSet<Gallery> Galleries { get; set; }

        public GrapesIndiaContext(DbContextOptions<GrapesIndiaContext> dbContextOptions) : base(dbContextOptions)
        {

        }

    }
}
