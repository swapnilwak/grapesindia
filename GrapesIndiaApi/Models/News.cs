﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Models
{
    public class News
    {
        [Key]
        public int NewsID { get; set; }
        public string NewsImageURL { get; set; }
        public string NewsTitleURL { get; set; }
        public string NewsDescription { get; set; }

    }
}
