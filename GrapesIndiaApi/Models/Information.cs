﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Models
{
    public class Information
    {
        [Key]
        public int InfoID { get; set; }
        public string InfoImage { get; set; }
        public string InfoTitle { get; set; }   
        public string InfoDescription { get; set; }
    }
}
