﻿using GrapesIndiaApi.Models;
using GrapesIndiaApi.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Persistance
{
    public class UserRepository : IUserRepository
    {
        private GrapesIndiaContext _grapesIndiaContext;

        public UserRepository(GrapesIndiaContext grapesIndiaContext)
        {
            _grapesIndiaContext = grapesIndiaContext;
        }
        public async Task<User> ValidateUserAsync(string username,string password)
        {
            User result = await _grapesIndiaContext.User.SingleOrDefaultAsync(u => string.Equals(u.Username, username, System.StringComparison.OrdinalIgnoreCase)
             && string.Equals(u.Password, password, System.StringComparison.OrdinalIgnoreCase));
            return result;
        }

        public async Task<List<User>> GetAllUserAsync()
        {
            List<User> result = await _grapesIndiaContext.User.ToListAsync();
            return result;
        }
    }
}
