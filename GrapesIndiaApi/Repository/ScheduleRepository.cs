﻿using GrapesIndiaApi.Models;
using GrapesIndiaApi.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Persistance
{
    public class ScheduleRepository : IScheduleRepository
    {
        private GrapesIndiaContext _grapesIndiaContext;

        public ScheduleRepository(GrapesIndiaContext grapesIndiaContext)
        {
            _grapesIndiaContext = grapesIndiaContext;
        }

        [HttpGet("getAllSchedules")]
        public async Task<List<Schedule>> GetAllSchedulesAsync()
        {
            return _grapesIndiaContext.Schedule.ToList();

         //  return await _grapesIndiaContext.Schedule.ToListAsync();
        }

        [HttpGet("getallschedules121")]
        public List<Schedule> getallschedule11()
        {
            return _grapesIndiaContext.Schedule.ToList();
        }
    }
}
