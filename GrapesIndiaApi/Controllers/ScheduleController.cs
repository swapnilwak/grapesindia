﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrapesIndiaApi.Models;
using GrapesIndiaApi.Repository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GrapesIndiaApi.Controllers
{
    [Route("api/Schedule")]
    [ApiController]
    public class  ScheduleController : ControllerBase
    {
        private IScheduleRepository _scheduleRepository;

        public ScheduleController(IScheduleRepository scheduleRepository)
        {
            _scheduleRepository = scheduleRepository;
        }

        // GET: api/Schedule
        [HttpGet("getAllSchedule")]
        public Task<List<Schedule>> GetAllSchedules()
        {
            return _scheduleRepository.GetAllSchedulesAsync();
        }

        [HttpGet("getAllSchedule11")]
        public List<Schedule> getallschedule11()
        {
            return _scheduleRepository.getallschedule11();
        }










        // GET: api/Schedule/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Schedule
        [HttpPost]
        public void PostSchedule([FromBody] string value)
        {
        }

        // PUT: api/Schedule/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
