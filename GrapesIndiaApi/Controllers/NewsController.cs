﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using GrapesIndiaApi.Models;

namespace GrapesIndiaApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NewsController : ControllerBase
    {
        private GrapesIndiaContext _grapesIndiaContext;

        public NewsController(GrapesIndiaContext grapesIndiaContext)
        {
            _grapesIndiaContext = grapesIndiaContext;
        }


        [HttpGet("GetNews")]
        public List<News> GetNews1()
        {
            return _grapesIndiaContext.NewsTable.ToList();
        }

        [HttpPost("PostNews")]
        public News PostData(News _news)
        {
            _grapesIndiaContext.NewsTable.Add(_news);
            _grapesIndiaContext.SaveChanges();

            return _news;
        }

    }
}