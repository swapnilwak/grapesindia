﻿using GrapesIndiaApi.Models;
using GrapesIndiaApi.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GrapesIndiaApi.Controllers
{
    [Route("api/User")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserRepository _userRepository;

        public UserController(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        // POST api/values
        [HttpPost("validateUser")]
        public async Task<User> ValidateUserAsync(string username,string password)
        {
           return await _userRepository.ValidateUserAsync(username, password);
        }

        [HttpGet("getAllUser")]
        public async Task<List<User>> GetUsers()
        {
            return await _userRepository.GetAllUserAsync();
        }

    }
}
